from bokeh.plotting import figure, output_file, show
import os

# prepare some data
x = [1, 2, 3, 4, 5]
y = [6, 7, 2, 4, 5]

# output to static HTML file
src = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
html_output = os.path.join(src, "html_output", "lines.html")
output_file(html_output)

# create a new plot with a title and axis labels
p = figure(title="simple line example", x_axis_label='x', y_axis_label='y')

# add a line renderer with legend and line thickness
p.line(x, y, legend="Temp.", line_width=2)

# show the results
show(p)